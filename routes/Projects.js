const express = require("express");
const router = express.Router();
const authenticateUser = require("../middleware/auth-middleware");
const {
  createProjectByUserId,
  getAllProjectsOfUser,
  getProjectDetails,
  updateProjectById,
  deletedProjectById,
} = require("../controllers/Projects");

router.post("/", authenticateUser, createProjectByUserId);

router.get("/", authenticateUser, getAllProjectsOfUser);

router.get("/:id", authenticateUser, getProjectDetails);

router.put("/:id", authenticateUser, updateProjectById);

router.delete("/:id", authenticateUser, deletedProjectById);

module.exports = router;
