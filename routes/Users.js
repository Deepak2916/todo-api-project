const express = require("express");
const router = express.Router();
const {
  CreateUser,
  GetUserByUserName,
  UpdateUserByUserName,
  loginUser
} = require("../controllers/Users");
const authenticateUser = require("../middleware/auth-middleware");

router.post("/signup", CreateUser);
router.post("/login", loginUser);
router.get("/:username", GetUserByUserName);
router.put("/:username", authenticateUser, UpdateUserByUserName);

module.exports = router;
