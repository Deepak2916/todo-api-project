const express = require("express");
const router = express.Router();
const authenticateUser = require("../middleware/auth-middleware");
const {
  createTaskByProjectId,
  updateTask,
  getTasksByProjectId,
  deleteTask,
} = require("../controllers/Tasks");

router.get("/project/:projectId/task", authenticateUser, getTasksByProjectId);
router.post("/project/:projectId/task", authenticateUser, createTaskByProjectId);
router.put("/task/:id", authenticateUser, updateTask);
router.delete("/task/:id", authenticateUser, deleteTask);

module.exports = router;
