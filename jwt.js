const jwt = require('jsonwebtoken')
const {SECRETKEY} = require('./config')

const generateJwtToken = (data)=>{
   return jwt.sign(data,SECRETKEY)
}

const verifyJwtToken = (token)=>{
    return  jwt.verify(token,SECRETKEY)
}

module.exports = {generateJwtToken,verifyJwtToken}