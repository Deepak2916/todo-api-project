const { verifyJwtToken } = require("../jwt");
const userModel = require("../models/Users");

const authenticateJWT = async (req, res, next) => {
  const token = req.headers.authorization;
  try {
    const { id } = verifyJwtToken(token);

    const user = await userModel.findOne({ where: { id } });
    if (user) {
      req.user = user;
      next();
    } else {
      next({
        error: "Unauthorized",
        message: "You do not have permission to access this resource.",
      });
    }
  } catch (error) {
    next({
      error: "Unauthorized",
      message: "You do not have permission to access this resource.",
    });
  }
};

module.exports = authenticateJWT;
