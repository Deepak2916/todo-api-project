const { DBConfigLink } = require("../config");

const Sequelize = require("sequelize");

const sequelize = new Sequelize(DBConfigLink, {
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
});
const testDbConnection = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};
testDbConnection();

module.exports = sequelize;
