# Todo API Documentation

This API provides endpoints to manage users, projects, and tasks for a todo list application.

## User Management

### Create User (Signup)

```
https://todo-api-project-deepak.onrender.com/api/user/signup
```

- Route: `/api/user/signup`
- Method: POST
- Sample Request:
  ```json
  {
    "name": "Deepak",
    "username": "deepak@24.2",
    "password": "password",
    "email": "deepak@example.com",
    "phoneNumber": "9391234567"
  }
  ```
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
      "id": 1,
      "name": "Deepak",
      "email": "deepak@example.com",
      "phoneNumber": "9391234567",
      "password": "$2b$10$Ir9udpG7cjp85D4O8HNQZu/0qmpaPREnmMH6ZKN.jFwzl6Q1vhA26",
      "username": "deepak@24.2",
      "updatedAt": "2023-08-19T08:46:45.174Z",
      "createdAt": "2023-08-19T08:46:45.174Z"
    }
  }
  ```
- Description: This route allows users to sign up by providing their name, username, password, email, and phone number.

### User Login

```
https://todo-api-project-deepak.onrender.com/api/user/login
```

- Route: `/api/user/login`
- Method: POST
- Sample Request:
  ```json
  {
    "username": "deepak@24.2",
    "password": "password"
  }
  ```
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
        "id": 1,
        "name": "Deepak",
        "email": "deepak@example.com",
        "password": "$2b$10$Ir9udpG7cjp85D4O8HNQZu/0qmpaPREnmMH6ZKN.jFwzl6Q1vhA26",
        "username": "deepak@24.2",
        "phoneNumber": "9391234567",
        "createdAt": "2023-08-19T08:46:45.174Z",
        "updatedAt": "2023-08-19T08:46:45.174Z",
        "projects": [
            {
                "id": 1,
                "title": "Sample Project",
                "description": "This is a sample project.",
                "isCompleted": false,
                "createdAt": "2023-08-19T08:58:48.201Z",
                "updatedAt": "2023-08-19T08:58:48.201Z",
                "userId": 1,
                "tasks": [
                    {
                        "id": 1,
                        "title": "Sample Taks",
                        "description": "This is a sample task.",
                        "isCompleted": false,
                        "createdAt": "2023-08-19T09:07:24.132Z",
                        "updatedAt": "2023-08-19T09:07:24.132Z",
                        "projectId": 1,
                        "userId": 1
                    }
                ]
            }
        ]
    },
    "jwtToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjkyNDM2NDc5fQ.BGNrQKd1rEElJeOG3aHEEzkugW2BgMY3z4AYEsEsovM"
  }
  ```
- Description: Users can log in using their username and password to receive an access token.

### Get User Details by Username

```
https://todo-api-project-deepak.onrender.com/api/user/deepak@24.2
```

- Route: `/api/user/{username}`
- Method: GET
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
        "id": 1,
        "name": "Deepak",
        "email": "deepak@example.com",
        "phone_number": "9391234567"
    }
  }
  ```
- Description: Retrieve user details by providing the username.

## Project Management

### Create Project

```
https://todo-api-project-deepak.onrender.com/api/project
```

- Route: `/api/project`
- Method: POST
- Headers: `Authorization:{jwt token}`
- Sample Request:
  ```json
  {
    "title": "Sample Project",
    "description": "This is a sample project."
  }
  ```
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
      "isCompleted": false,
      "id": 1,
      "title": "Sample Project",
      "description": "This is a sample project.",
      "userId": 1,
      "updatedAt": "2023-08-19T08:58:48.201Z",
      "createdAt": "2023-08-19T08:58:48.201Z"
    }
  }
  ```
- Description: Create a new project with a title and description.

### Get Project Details

```
https://todo-api-project-deepak.onrender.com/api/project
```

- Route: `/api/project`
- Method: GET
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
  {
      "success": true,
      "data": [
          {
              "id": 1,
              "title": "Sample Project",
              "description": "This is a sample project.",
              "isCompleted": false,
              "createdAt": "2023-08-19T08:58:48.201Z",
              "updatedAt": "2023-08-19T08:58:48.201Z",
              "userId": 1,
              "tasks": [
                  {
                      "id": 1,
                      "title": "Sample Taks",
                      "description": "This is a sample task.",
                      "isCompleted": false,
                      "createdAt": "2023-08-19T09:07:24.132Z",
                      "updatedAt": "2023-08-19T09:07:24.132Z",
                      "projectId": 1,
                      "userId": 1
                  }
              ]
          }
      ]
    
  }
  ```
- Description: Retrieve details of all projects associated with the user.

### Get Single Project Details

```
https://todo-api-project-deepak.onrender.com/api/project/1
```

- Route: `/api/project/{project id}`
- Method: GET
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
  {
    "success": true,
    "data": [
        {
            "id": 1,
            "title": "Sample Project",
            "description": "This is a sample project.",
            "isCompleted": false,
            "createdAt": "2023-08-19T08:58:48.201Z",
            "updatedAt": "2023-08-19T08:58:48.201Z",
            "userId": 1,
            "tasks": [
                {
                    "id": 1,
                    "title": "Sample Taks",
                    "description": "This is a sample task.",
                    "isCompleted": false,
                    "createdAt": "2023-08-19T09:07:24.132Z",
                    "updatedAt": "2023-08-19T09:07:24.132Z",
                    "projectId": 1,
                    "userId": 1
                }
            ]
        }
    ]
  }
  ```
- Description: Retrieve details of a specific project by providing its ID.

### Update Project Details

```
https://todo-api-project-deepak.onrender.com/api/project/1
```

- Route: `/api/project/{project id}`
- Method: PUT
- Headers: `Authorization:{jwt token}`
- Sample Request:
  ```json
  {
    "title": "Updated Project Title",
    "description": "This is the updated project description."
  }
  ```
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
        "id": 1,
        "title": "Updated Project Title",
        "description": "This is the updated project description.",
        "isCompleted": false,
        "createdAt": "2023-08-19T08:58:48.201Z",
        "updatedAt": "2023-08-19T09:18:54.607Z",
        "userId": 1
    }
  }
  ```
- Description: Update the title and/or description of a project.

### Delete Project

```
https://todo-api-project-deepak.onrender.com/api/project/1
```

- Route: `/api/project/{project id}`
- Method: DELETE
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
  {
    "success": true,
    "message": "Project deletion was successful"
  }
  ```
- Description: Delete a project by providing its ID.

## Task Management

### Create Task

```
https://todo-api-project-deepak.onrender.com/api/project/1/task
```

- Route: `/api/task/project/{project id}`
- Method: POST
- Headers: `Authorization: {jwt token}`
- Sample Request:
  ```json
  {
    "title": "Sample Task",
    "description": "This is a sample task."
  }
  ```
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
        "isCompleted": false,
        "id": 1,
        "title": "Sample Taks",
        "description": "This is a sample task.",
        "userId": 1,
        "projectId": 1,
        "updatedAt": "2023-08-19T09:07:24.132Z",
        "createdAt": "2023-08-19T09:07:24.132Z"
    }
  } 
  ```
- Description: Create a new task within a specific project.

### Get Tasks of Project

```
https://todo-api-project-deepak.onrender.com/api/project/1/task
```

- Route: `/api/task/project/{project id}`
- Method: GET
- Headers: `Authorization: {jwt token}`
- Sample Response:
  ```json
  {
    "success": false,
    "data": [
        {
            "id": 1,
            "title": "Sample Taks",
            "description": "This is a sample task.",
            "isCompleted": false,
            "createdAt": "2023-08-19T09:07:24.132Z",
            "updatedAt": "2023-08-19T09:07:24.132Z",
            "projectId": 1,
            "userId": 1
        }
    ]
  }
  ```
- Description: Retrieve all tasks within a specific project.

### Update Task Details

```
https://todo-api-project-deepak.onrender.com/api/task/1
```

- Route: `/api/task/{task id}`
- Method: PUT
- Headers: `Authorization: {jwt token}`
- Sample Request:
  ```json
  {
    "title": "Updated Task Title",
    "description": "This is the updated task description."
  }
  ```
- Sample Response:
  ```json
  {
    "success": true,
    "data": {
        "id": 1,
        "title": "Updated Task Title",
        "description": "This is the updated task description.",
        "isCompleted": false,
        "createdAt": "2023-08-19T09:07:24.132Z",
        "updatedAt": "2023-08-19T09:17:44.057Z",
        "projectId": 1,
        "userId": 1
    }
  }
  ```
- Description: Update the title and/or description of a task.

### Delete Task

```
https://todo-api-project-deepak.onrender.com/api/task/1
```

- Route: `/api/task/{task id}`
- Method: DELETE
- Headers: `Authorization: {jwt token}`
- Sample Response:
  ```json
  {
    "success": true,
    "message": "Task deletion was successful"
  }
  ```
- Description: Delete a task by providing its ID.
