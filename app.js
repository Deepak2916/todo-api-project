const express = require("express");
const cors = require("cors");
const sequelize = require("./database/DatabasePg");
const userModel = require("./models/Users");
const projectModel = require("./models/Projects");
const taskModel = require("./models/Tasks");

const userRouter = require("./routes/Users");
const projectRouter = require("./routes/Projects");
const taskRouter = require("./routes/Tasks");
const errorMiddleware = require("./middleware/errorMiddleware");
const app = express();

const { PORT } = require("./config");
app.use(cors());
app.use(express.json());

userModel.hasMany(projectModel, { constraints: true, onDelete: "CASCADE" });
projectModel.hasMany(taskModel, { constraints: true, onDelete: "CASCADE" });
userModel.hasMany(taskModel, { constraints: true, onDelete: "CASCADE" });

app.use("/api/user", userRouter);
app.use("/api/project", projectRouter);
app.use("/api", taskRouter);

app.use((req, res, next) => {
  next({ message: "Sorry, the page you're looking for doesn't exist." });
});
app.use(errorMiddleware);

// sequelize.sync({force:true})
sequelize.sync().then((res) => {
  app.listen(process.env.PORT || 3000, () => {
    console.log("server is running");
  });
});
