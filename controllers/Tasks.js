const { Op } = require("sequelize");
const projectModel = require("../models/Projects");
const taskModel = require("../models/Tasks");

const createTaskByProjectId = async (req, res, next) => {
  const { title, description } = req.body;
  const { projectId } = req.params;
  const user = req.user;
  try {
    const project = await projectModel.findOne({ where: { id: projectId } });

    if (project && project.userId === user.id) {
      if (project.isCompleted) {
        await projectModel.update(
          { isCompleted: false },
          { where: { id: projectId } }
        );
      }
      const task = await project.createTask({
        title,
        description,
        userId: user.id,
      });
      res.status(200).json({
        success: true,
        data: task,
      });
    } else {
      next({
        success: false,
        message: "Project not found.",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Task creation was not successful.",
      error: error.message,
    });
  }
};

const updateTask = async (req, res, next) => {
  const { id } = req.params;

  try {
    const task = await taskModel.findOne({ where: { id } });

    if (task && task.userId === req.user.id) {
      const [rowsUpdated, updatedTasks] = await taskModel.update(req.body, {
        where: { id: id },
        returning: true,
      });
      const task = updatedTasks[0].dataValues;

      const isAllTasksUpdated = await taskModel.findAll({
        where: {
          [Op.and]: [{ isCompleted: false }, { projectId: task.projectId }],
        },
      });
      if (isAllTasksUpdated.length == 0) {
        await projectModel.update(
          { isCompleted: true },
          { where: { id: task.projectId } }
        );
      }
      res.status(200).json({
        success: true,
        data: task,
      });
    } else {
      next({
        success: false,
        message: "Task does not exist.",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Task update could not be completed successfully.",
      error: error.message,
    });
  }
};

const getTasksByProjectId = async (req, res, next) => {
  const { projectId } = req.params;
  try {
    const project = await projectModel.findOne({ where: { id: projectId } });
    if (project && project.userId === req.user.id) {
      const tasks = await project.getTasks();
      res.status(200).json({
        success: false,
        data: tasks,
      });
    } else {
      next({
        success: true,
        message: "Project does not exist.",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Retrieving the tasks was unsuccessful.",
      error: error.message,
    });
  }
};

const deleteTask = async (req, res, next) => {
  const { id } = req.params;
  try {
    const task = await taskModel.findOne({ where: { id } });

    if (task && task.userId === req.user.id) {
      await taskModel.destroy({ where: { id } });

      res.status(200).json({
        success: true,
        message: "Task deletion was successful",
      });
    } else {
      next({
        success: false,
        message: "Task does not exist.",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Task deletion was unsuccessful.",
      error: error.message,
    });
  }
};

module.exports = {
  createTaskByProjectId,
  updateTask,
  getTasksByProjectId,
  deleteTask,
};
