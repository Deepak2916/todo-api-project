const Project = require("../models/Projects");
const Task = require("../models/Tasks");

const createProjectByUserId = async (req, res, next) => {
  const { title, description } = req.body;

  try {
    const user = req.user;

    const project = await user.createProject({ title, description });
    res.status(200).json({
      success: true,
      data: project,
    });
  } catch (error) {
    next({
      success: false,
      message: "Project creation was not successful.",
      error: error.message,
    });
  }
};

const getAllProjectsOfUser = async (req, res, next) => {
  try {
    const user = req.user;

    const projects = await user.getProjects({
      include: [
        {
          model: Task,
        },
      ],
    });
    res.status(200).json({
      success: true,
      data: projects,
    });
  } catch (error) {
    next({
      success: false,
      message: "Retrieving the projects was unsuccessful.",
      error: err.message,
    });
  }
};

const getProjectDetails = async (req, res, next) => {
  const { id } = req.params;
  const user = req.user;

  try {
    const projectDetails = await user.getProjects({
      where: { id },
      include: [
        {
          model: Task,
        },
      ],
    });
    if (projectDetails.length) {
      res.status(200).json({
        success: true,
        data: projectDetails,
      });
    } else {
      next({
        success: false,
        message: "project not found.",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Retrieving the projects was unsuccessful.",
      error: error.message,
    });
  }
};

const updateProjectById = async (req, res, next) => {
  const { id } = req.params;
  const user = req.user;
  try {
    const project = await Project.findOne({ where: { id } });
    if (project && project.userId === user.id) {
      const [rowsUpdated, updatedProject] = await Project.update(req.body, {
        where: { id: id },
        returning: true,
      });

      res.status(200).json({
        success: true,
        data: updatedProject[0].dataValues,
      });
    } else {
      next({
        success: false,
        message: "Project not fount",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "project update could not be completed successfully.",
      error: error.message,
    });
  }
};

const deletedProjectById = async (req, res, next) => {
  const { id } = req.params;
  const user = req.user;
  try {
    const project = await Project.findOne({ where: { id } });

    if (project && project.userId === user.id) {
      await user.removeProject(project);

      res.status(200).json({
        success: true,
        message: "Project deletion was successful",
      });
    } else {
      next({
        success: false,
        message: "Project not found",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Project deletion was unsuccessful",
      error: error.message,
    });
  }
};

module.exports = {
  createProjectByUserId,
  getProjectDetails,
  getAllProjectsOfUser,
  updateProjectById,
  deletedProjectById,
};
