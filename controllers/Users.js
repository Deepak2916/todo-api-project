const { generateJwtToken } = require("../jwt");
const userModel = require("../models/Users");
const bcrypt = require("bcrypt");
const Projects = require("../models/Projects");
const Tasks = require("../models/Tasks");
const CreateUser = async (req, res, next) => {
  let { name, email, phoneNumber, password, username } = req.body;

  try {
    const hashedPassword = await bcrypt.hash(password, 10);
    let user = await userModel.create({
      name,
      email,
      phoneNumber,
      password: hashedPassword,
      username,
    });

    res.status(200).json({
      success: true,
      data: user,
    });
  } catch (error) {
    next({
      success: false,
      message: "User creation was not successful.",
      error: error.message,
    });
  }
};

const GetUserByUserName = async (req, res, next) => {
  const { username } = req.params;

  try {
    const user = await userModel.findOne({ where: { username: username } });

    if (user) {
      res.status(200).json({
        success: true,
        data: {
          id: user.id,
          name: user.name,
          email: user.email,
          phone_number: user.phoneNumber,
        },
      });
    } else {
      next({
        success: false,
        message: "User not found.",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Retrieving the user details was not successful.",
      error: err.message,
    });
  }
};

const UpdateUserByUserName = async (req, res, next) => {
  const { username } = req.params;
  const { password } = req.body;

  try {
    let requestedUserData = req.body;
    if (password) {
      const hashedPassword = await bcrypt.hash(password, 10);
      requestedUserData = { ...requestedUserData, password: hashedPassword };
    }
    const [rowsUpdated, updatedUser] = await userModel.update(
      requestedUserData,
      {
        where: {
          username,
        },
        returning: true,
      }
    );
    if (rowsUpdated) {
      res.status(200).json({
        success: true,
        data: updatedUser[0].dataValues,
      });
    } else {
      next({
        success: false,
        message: "user does not exist",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "user update could not be completed successfully.",
      error: error.message,
    });
  }
};

const loginUser = async (req, res, next) => {
  const { password, username } = req.body;
  try {
    const user = await userModel.findOne({
      where: { username: username },
      include: [
        {
          model: Projects,
          include: [Tasks],
        },
      ],
    });

    const isPasswordCorrect =
      user && (await bcrypt.compare(password, user.password));
    if (isPasswordCorrect) {
      //JWT token
      const jwtToken = generateJwtToken({ id: user.id });

      res.status(200).json({
        success: true,
        data: user,
        jwtToken,
      });
    } else {
      next({
        success: false,
        message: "Entered invalid username or password.",
      });
    }
  } catch (error) {
    next({
      success: false,
      message: "Retrieving the user details was not successful.",
      error: error.message,
    });
  }
};

module.exports = {
  CreateUser,
  GetUserByUserName,
  UpdateUserByUserName,
  loginUser,
};
